#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("range-based-for")
{
    SECTION("stl containers")
    {
        vector<string> vec = {"1", "2", "3"};

        for(const auto& item : vec)
        {
            cout << item << " ";
        }
        cout << "\n";

        SECTION("is interpreted as")
        {
            for(auto it = vec.begin(); it != vec.end(); ++it)
            {
                const auto& item = *it;

                cout << item << " ";
            }
            cout << "\n";
        }
    }

    SECTION("static array")
    {
        int tab[10] = { 1, 2, 3, 4, 5 };

        for(const auto& item : tab)
        {
            cout << item << " ";
        }
        cout << "\n";

        SECTION("is interpreted as")
        {
            for(auto it = begin(tab); it != end(tab); ++it)
            {
                const auto& item = *it;

                cout << item << " ";
            }
            cout << "\n";
        }
    }

    SECTION("initializer list")
    {
        for(const auto& item : {1, 2, 3})
        {
            cout << item << " ";
        }
        cout << "\n";
    }
}
