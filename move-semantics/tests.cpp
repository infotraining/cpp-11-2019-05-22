#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

string full_name(const string& first, const string& last)
{
    return first + " " + last;
}

vector<string> create_words()
{
    vector<string> vec;

    string name = "Ewa";

    vec.push_back(name);
    vec.push_back(name + " Kowalska");
    vec.push_back(full_name(name, "Nowak"));
    vec.push_back("Ola Nowak");
    vec.push_back(std::move(name));

    return vec;
}

TEST_CASE("reference bindings")
{
    string name = "Jan";

    SECTION("C++98")
    {
        string& ref_name = name;

        const string& cref_full_name = full_name(name, "Kowalski");

        //cref_full_name[0] = 'P';
    }

    SECTION("C++11")
    {
        string&& refref_full_name = full_name(name, "Kowalski");

        refref_full_name[0] = 'P';

        REQUIRE(refref_full_name == "Pan Kowalski"s);

        // string&& refref = name; // ERROR - ill formed
    }
}

TEST_CASE("vector returned from function")
{
    vector<string> words = create_words();
}

void may_throw()
{

}

namespace kplus
{
    template <typename T>
    class K_Vector_c
    {
        T* items_;
        size_t size_;

        using iterator = T*;
        using const_iterator = const T*;

    public:
        explicit K_Vector_c(size_t size)
            : items_(new T[size])
            , size_(size)
        {
            fill_n(items_, size_, T{});
            cout << "K_Vector_c(size: " << size_ << ", items: " << items_ << ")\n";
        }

        K_Vector_c(const K_Vector_c& other) : items_{new T[other.size_]}, size_{other.size_}
        {
            copy(other.begin(), other.end(), items_);
            cout << "K_Vector_c(cc size: " << size_ << ", items: " << items_ << ")\n";
        }

        // move constructor
        K_Vector_c(K_Vector_c&& source) noexcept
            : items_(source.items_)
            , size_(source.size_)
        {
            try
            {
                may_throw();
            }
            catch(...)
            {
                //
            }
            cout << "K_Vector_c(mv size: " << size_ << ", items: " << items_ << ")\n";
            source.items_ = nullptr;
        }

        // move assignment
        K_Vector_c& operator=(K_Vector_c&& source)
        {
            if (this != &source)
            {
                delete[] items_;

                items_ = source.items_;
                size_ = source.size_;

                source.items_ = nullptr;
            }

            return *this;
        }

        ~K_Vector_c()
        {
            cout << "~K_Vector_c(size: " << size_ << ", items: " << items_ << ")\n";
            delete[] items_;
        }

        T& operator[](size_t index)
        {
            return items_[index];
        }

        iterator begin()
        {
            return items_;
        }

        const_iterator begin() const
        {
            return items_;
        }

        iterator end()
        {
            return items_ + size_;
        }

        const_iterator end() const
        {
            return items_ + size_;
        }
    };
}

kplus::K_Vector_c<string> load_data()
{
    kplus::K_Vector_c<string> data(1'000'000);

    data[0] = "First";
    data[1] = "Second";

    return data;
}

namespace kplus
{
    struct K_Data_c
    {
        K_Vector_c<int> items;
        vector<string> vec;
        string s;
    };
}

TEST_CASE("K_Vector_c")
{
    using namespace kplus;

    K_Vector_c<int> vec(10);
    vec[0] = 1;
    vec[5] = 5;

    for (const auto& item : vec)
        cout << item << " ";
    cout << '\n';

    cout << "\n----------\n";

    K_Vector_c<string> vec2 = load_data();
    //K_Vector_c<string> backup = vec2;

    vector<K_Vector_c<string>> data;
    data.push_back(load_data());
    data.push_back(std::move(vec2));
}

struct Data
{
    vector<int> vec;
    string s;
    int a;
};

TEST_CASE("Data & move semantics")
{
    Data d{{1, 2, 3}, "text", 42};

    Data d_target = std::move(d);

    REQUIRE(d.s == ""s);
    REQUIRE(d_target.s == "text"s); 
}

template <typename T>
void deduce(T&& value)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("universal reference")
{
    string str = "text";
    deduce(str); // because l-value is passed as arg - arg type is T& (due to collapsing)

    deduce(string("text")); // because r-value is passed - arg is T&& (no collapsing)
}

template <typename F, typename Arg>
void callable_wrapper(F fun, Arg&& arg /*forwarding ref*/)
{
    puts(__PRETTY_FUNCTION__);

    cout << "callable_wrapper - called...\n";

    fun(std::forward<Arg>(arg));
}

template <typename T, typename Arg>
std::unique_ptr<T> my_make_unique(Arg&& arg)
{
    return std::unique_ptr<T>(new T(std::forward<Arg>(arg)));
}

void foo(const string& text)
{
    cout << "foo(" << text << ")\n";
}

TEST_CASE("perfect forward")
{
    callable_wrapper(foo, "hello");
}

TEST_CASE("push_backs to vector")
{
    cout << "\n----\n";

    vector<kplus::K_Vector_c<int>> vec;

    vec.push_back(kplus::K_Vector_c<int>(1));
    cout << "---\n";
    vec.push_back(kplus::K_Vector_c<int>(2));
    cout << "---\n";
    vec.push_back(kplus::K_Vector_c<int>(3));
    cout << "---\n";
    vec.push_back(kplus::K_Vector_c<int>(4));
    cout << "---\n";
    vec.push_back(kplus::K_Vector_c<int>(5));
    cout << "---\n";
    vec.push_back(kplus::K_Vector_c<int>(6));
}

