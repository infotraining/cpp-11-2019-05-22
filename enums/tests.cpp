#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <any>

#include "catch.hpp"

using namespace std;

enum class Coffee : uint8_t { espresso = 1, cappucino, latte };

TEST_CASE("scoped enums")
{
    Coffee cf1 = Coffee::cappucino;

    int value = static_cast<int>(cf1);

    value = 100;

    Coffee cf2 = static_cast<Coffee>(value);
}

struct Point
{
    int x, y;

    Point(int x, int y) : x(x), y(y)
    {}
};

struct Aggregate
{
    int x;
    double y;
    string s;
};

unsigned long gen()
{
    return 42;
}

struct Data
{
    vector<int> data;

    Data()
    {
        cout << "Default ctor for Data()\n";
    }

    explicit Data(size_t size) : data(size, -1)
    {
    }

    explicit Data(int size, int value) : data{size, value}
    {
    }

    Data(std::initializer_list<int> il)
        : data(il)
    {
    }

    void append(std::initializer_list<int> il)
    {
//        for(const auto& item : il)
//            data.push_back(item);

        data.insert(data.end(), il);
    }
};

TEST_CASE("Data & constructor")
{
    Data dzero{};
    Data d0a{10}; // Data[10]
    Data d0b(4); // Data[-1, -1, -1, -1]

    Data d1(10, 1);
    Data d2{10, 1};

    Data d3 = {1, 2, 3, 4, 5}; // constructor with initializer_list
}

template <typename T>
void foo(initializer_list<T> arg)
{

}

TEST_CASE("init syntax")
{
    foo({1, 2, 3});
    auto il = {1, 2, 3};

    SECTION("C++03")
    {
        Point pt(1, 3);
        Point pt2 = Point(1, 2);
    }

    SECTION("C++11")
    {
        int x1{42}; // int x1 = 42;
        int x2{}; // int x2 = 0;
        int* ptr{}; // int* ptr = nullptr;

        Aggregate agg1{1, 2, "text"};
        Aggregate agg2{1, 2};
        Aggregate agg3{};
        REQUIRE(agg2.s == ""s);

        Point pt1{1, 3}; // Point pt2(1, 3);

        int value{gen()};

        vector<int> vec = { 1, 2, 3, 4 }; // vector<int>(std::initializer_list<int>)
        map<int, string> dict = { {1, "one"}, {2, "two"} };

        vector<int> vec1(3, -1); // [-1, -1, -1]
        vector<int> vec2{10, -1}; // [10, -1]

        vec2.insert(begin(vec2), {10, 20, 3, 4});

        for(const auto& item : vec2)
            cout << item << " ";
        cout << endl;

        auto il = {1, 2, 3}; // initializer_list<int>
        vector<int> vec3 = il;
        vec2 = il;

        vector<any> vec_any = { 1, "text", 3.44 };

        // initializer_list does not work properly with unique_ptrs
        //vector<unique_ptr<int>> vec_ptrs = { make_unique<int>(13), make_unique<int>(42) };
    }
}
