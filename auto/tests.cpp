#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <list>

#include "catch.hpp"

using namespace std;

TEST_CASE("auto")
{
    auto ax1 = 1u; // int
    auto ax2 = 3.14; // double
    auto ax3 = 3.14f; // float
    //auto ax4 = static_cast<char>(1); // int
    char c4 = 1;

    //ax = "text"; // ERROR

    const std::map<int, string> dict;

    auto it = dict.begin();

    SECTION("auto in container")
    {
        //vector vec = {1, 2, 3, 4 }; // CTAD - since C++17
        vector<int> vec = { 1, 2, 3, 4 };

        for(auto it = vec.cbegin(); it != vec.cend(); ++it)
        {
            cout << *it << " ";
        }
        cout << "\n";
    }
}

template <typename T, size_t N>
constexpr size_t size(T (&arr)[N])
{
    return N;
}

TEST_CASE("size of array")
{
    int tab[] = {1, 2, 3};
    REQUIRE(size(tab) == 3);

    static_assert(size(tab) == 3, "array must have size 3");

    int backup[size(tab)];
}

void foo(int x)
{
}

template <typename T>
void deduce1(T arg)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void deduce2(T& arg)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("auto & type deduction")
{
    int x = 10;
    const int cx = 42;
    int& ref_x = x;
    const int& cref_x = x;
    int tab[3] = {1, 2, 3};

    SECTION("case 1")
    {
        deduce1(1);
        auto ax1 = 1;

        deduce1(3.14);
        auto ax2 = 3.14;

        deduce1(3.14f);
        auto ax3 = 3.14f;

        deduce1(x);
        auto ax4 = x; //int

        deduce1(ref_x);
        auto ax5 = ref_x; // int - ref is stripped away

        deduce1(cref_x);
        auto ax6 = cref_x; // int - const ref is stripped away

        deduce1(tab);
        auto ax7 = tab; // int* - array decays to pointer

        deduce1(foo);
        auto ax8 = foo; // void(*)(int) - function decays to pointer
    }

    SECTION("case 2")
    {
        cout << "\n\n";

        auto& aref1 = x; // int&
        deduce2(x);

        auto& aref2 = cx; // const int&
        deduce2(cx);

        auto& aref3 = ref_x; // int&
        deduce2(ref_x);

        auto& aref4 = cref_x; // const int&
        deduce2(cref_x);

        auto& aref5 = tab; // int(&)[3]
        deduce2(tab);

        auto& aref6 = foo; // void(&)(int)
        deduce2(foo);
    }
}

TEST_CASE("decltype")
{
    std::map<int, string> dict = { {1, "one"}, {2, "two"} };

    auto dict2 = dict;
    REQUIRE(dict2.size() == 2);

    decltype(dict) dict3;
    REQUIRE(dict3.size() == 0);

    static_assert(is_same_v<decltype(dict2),map<int, string>>, "Error");

    decltype(dict)::value_type item; // std::pair<const int, string>

    string value;
    decltype(dict[0]) ref_item = value; // string&
    REQUIRE(dict.size() == 2);
}

void foo(int, int)
{}

int foo(double, double)
{}

template <typename T>
auto multiply(T x, T y)
{
    return foo(x, y);
}

TEST_CASE("new function syntax")
{
    static_assert(is_void_v<decltype(multiply<int>(1, 2))>, "Error");
}



template <typename Map>
decltype(auto) lookup_by_ref(Map& m, const string& key)
{
    return m.at(key);
}

template <typename Map>
auto lookup_by_value(Map& m, const string& key)
{
    return m.at(key);
}

TEST_CASE("decltype(auto)")
{
    map<string, int> dict = { { "one", 1 } };

    lookup_by_ref(dict, "one"s) = 10;
}
