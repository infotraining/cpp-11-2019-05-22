#include <iostream>
#include <type_traits>
#include <vector>

using namespace std;

int x;
const int cx = 1;
int tab[10] = {};
vector<int> vec = {1, 2, 3};
const vector<int> cvec = {1, 2, 3};

using TODO = void;

void ptr_and_const()
{
    int x = 10;
    int y = 42;

    const int* ptr_to_const = &x; // pointer to const
    int const* ptr_to_const_eastconst = &x; // pointer to const

    //(*ptr_to_const)++; // ERROR
    ptr_to_const = &y;

    int* const const_ptr_to_var = &x; // const pointer
    (*const_ptr_to_var)++;
    //const_ptr_to_var = &y; // ERROR

    const int *const const_ptr_to_const = &x;
    int const* const const_ptr_to_const_eastconst = &x;

    int const cx_eastconst = 10;
}

void change_in_std_cpp17()
{
    auto x1(1); // int
    auto x2 = 1; // int

    auto x3{1}; // since C++17 - int [before C++17 - initializer_list<int>]
}

int main()
{
    auto a1 = x;
    static_assert(is_same<int, decltype(a1)>::value, "Error");

    auto a2 = &x;
    static_assert(is_same<int*, decltype(a2)>::value, "Error");

    const auto a3 = &x;
    static_assert(is_same<int* const, decltype(a3)>::value, "Error");

    auto* a4 = &x;
    static_assert(is_same<int*, decltype(a4)>::value, "Error");

    auto a5 = &cx;
    static_assert(is_same<int const*, decltype(a5)>::value, "Error");

    auto* a6 = &cx;
    static_assert(is_same<int const*, decltype(a6)>::value, "Error");

    int& rx = x;
    const int& crx = x;

    const auto& a7 = rx;
    static_assert(is_same<int const&, decltype(a7)>::value, "Error");

    auto a8 = crx;
    static_assert(is_same<int, decltype(a8)>::value, "Error");

    auto& a9 = crx;
    static_assert(is_same<int const&, decltype(a9)>::value, "Error");

    auto a10 = tab;
    static_assert(is_same<int *, decltype(a10)>::value, "Error");

    auto& a11 = tab;
    static_assert(is_same<int(&)[10], decltype(a11)>::value, "Error");

    auto a12 = vec.begin();
    static_assert(is_same<vector<int>::iterator, decltype(a12)>::value, "Error");

    auto a13 = vec.cbegin();
    static_assert(is_same<vector<int>::const_iterator, decltype(a13)>::value, "Error");

    auto a14 = vec.back();
    static_assert(is_same<int, decltype(a14)>::value, "Error");

    auto& a15 = cvec.back();
    static_assert(is_same<int const&, decltype(a15)>::value, "Error");

    const auto a16 = vec[0];
    static_assert(is_same<int const, decltype(a16)>::value, "Error");

     auto&& a17 = 7.5;
     static_assert(is_same<double&&, decltype(a17)>::value, "Error");

     auto&& a18 = crx;
     static_assert(is_same<int const&, decltype(a18)>::value, "Error");

     auto&& a19 = vec.front();
     static_assert(is_same<int&, decltype(a19)>::value, "Error");

    auto a20 = vec.begin();
    static_assert(is_same<vector<int>::iterator, decltype(a20)>::value, "Error");

    vector<int> vec = { 1, 2, 3, 4 };

    for(auto& item : vec)
        ++item;

    for(const auto& item : vec)
        cout << item << " ";
    cout << endl;

    vector<bool> flags { 0, 1, 0, 1 };

    for(auto&& bit : flags)
        bit.flip();

    for(const auto& bit : flags)
        cout << bit << " ";
    cout << endl;
}
