#ifndef PARAGRAPH_HPP_
#define PARAGRAPH_HPP_

#include <cstdlib>
#include <cstring>
#include <iostream>

namespace LegacyCode
{
    class Paragraph
    {
        char* buffer_;

    public:
        Paragraph() : buffer_(new char[1024])
        {
            std::strcpy(buffer_, "Default text!");
        }

        Paragraph(const Paragraph& p) : buffer_(new char[1024])
        {
            std::strcpy(buffer_, p.buffer_);
        }

        Paragraph(Paragraph&& other) : buffer_{nullptr}
        {
            std::swap(buffer_, other.buffer_);
        }

        Paragraph(const char* txt) : buffer_(new char[1024])
        {
            std::strcpy(buffer_, txt);
        }

        Paragraph& operator=(const Paragraph& p)
        {                 
            Paragraph temp(p);
            swap(temp);

            return *this;
        }

        Paragraph& operator=(Paragraph&& p)
        {
            Paragraph temp = std::move(p);
            swap(temp);

            return *this;
        }

        void swap(Paragraph& p)
        {
            std::swap(buffer_, p.buffer_);
        }


        void set_paragraph(const char* txt)
        {
            std::strcpy(buffer_, txt);
        }

        const char* get_paragraph() const
        {
            return buffer_;
        }

        void render_at(int posx, int posy) const
        {
            std::cout << "Rendering text '" << buffer_ << "' at: [" << posx << ", " << posy << "]" << std::endl;
        }

        virtual ~Paragraph()
        {
            delete[] buffer_;
        }
    };

    void swap(Paragraph& a, Paragraph& b)
    {
        a.swap(b);
    }
}

//void use()
//{
//    std::vector<Paragraph> vec;

//    std::sort(vec.begin(), vec.end());
//}

class Shape
{
    std::string name_ = "Shape";
public:   
//    Shape(const Shape&) = default;
//    Shape& operator=(const Shape&) = default;
//    Shape(Shape&&) = default;
//    Shape& operator=(Shape&&) = default;
    virtual ~Shape() = default;
    virtual void draw() const = 0;    
};

class Text : public Shape
{
    int x_, y_;
    LegacyCode::Paragraph p_;
public:
    Text(int x, int y, const std::string& text) : x_{x}, y_{y}, p_{text.c_str()}
    {}

    void draw() const override
    {
        p_.render_at(x_, y_);
    }

    std::string text() const
    {
        const char* txt = p_.get_paragraph(); 
        return (txt == nullptr) ? "" : txt;
    }

    void set_text(const std::string& text)
    {
        p_.set_paragraph(text.c_str());
    }
};

struct MoveOnly
{
    std::vector<std::string> data;

    MoveOnly(MoveOnly&&) = default;
    MoveOnly& operator=(MoveOnly&&) = default;
};

MoveOnly create_data()
{
    MoveOnly mo{{"1", "2", "3", "4"}};

    return mo;
}

void use(MoveOnly mo)
{

}

void consume()
{
    MoveOnly mo = create_data();

    use(std::move(mo));
    use(create_data());
    use(mo);
}

struct X
{
    string text;
    
    explicit X(string t) : text(std::move(t))
    {        
    }
};

void use_of_X()
{
    X x1(string("text"));
    
    string text = "abc";
    X x2(text);
}

namespace Optimum
{
void f(const X& x)
{
    vector<X> vec;
    
    vec.push_back(x);
}

void f(X&& x)
{
    vector<X> vec;
    
    vec.push_back(std::move(x));
}


}

void f(X x)
{
    vector<X> vec;
    
    vec.push_back(std::move(x));
}

#endif /*PARAGRAPH_HPP_*/


