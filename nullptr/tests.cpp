#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <cassert>

#include "catch.hpp"

using namespace std;

void foo(int* ptr)
{
    if (ptr != nullptr)
    {
        cout << "foo(int*: " << *ptr << ")\n";
    }
    else
    {
        cout << "foo(int*: null pointer)\n";
    }
}

void foo(nullptr_t)
{
    cout << "foo(nullptr_t)\n";
}

//void foo(long x)
//{
//    cout << "foo(long: " << x << ")\n";
//}

void foo(int x)
{
    cout << "foo(int: " << x << ")\n";
}


TEST_CASE("nullptr")
{
    int x = 10;

    foo(&x);
    //foo(NULL); // deprecated

    foo(nullptr);

    int* ptr = nullptr;
    foo(ptr);

    std::shared_ptr<int> sptr = nullptr;
    std::unique_ptr<int> uptr = nullptr;

    assert(sptr == nullptr);
}

enum Coffee : uint8_t { espresso, latte, cappucino };

struct Breakfast
{
    Coffee c1;
    Coffee c2;
};

TEST_CASE()
{

}
