# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/dev/trainings/cpp-11-2019-05-22/nullptr

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/dev/trainings/cpp-11-2019-05-22/nullptr/qtc-build

# Include any dependencies generated for this target.
include CMakeFiles/nullptr.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/nullptr.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/nullptr.dir/flags.make

CMakeFiles/nullptr.dir/main.cpp.o: CMakeFiles/nullptr.dir/flags.make
CMakeFiles/nullptr.dir/main.cpp.o: ../main.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/dev/trainings/cpp-11-2019-05-22/nullptr/qtc-build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/nullptr.dir/main.cpp.o"
	/usr/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/nullptr.dir/main.cpp.o -c /home/dev/trainings/cpp-11-2019-05-22/nullptr/main.cpp

CMakeFiles/nullptr.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/nullptr.dir/main.cpp.i"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/dev/trainings/cpp-11-2019-05-22/nullptr/main.cpp > CMakeFiles/nullptr.dir/main.cpp.i

CMakeFiles/nullptr.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/nullptr.dir/main.cpp.s"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/dev/trainings/cpp-11-2019-05-22/nullptr/main.cpp -o CMakeFiles/nullptr.dir/main.cpp.s

CMakeFiles/nullptr.dir/main.cpp.o.requires:

.PHONY : CMakeFiles/nullptr.dir/main.cpp.o.requires

CMakeFiles/nullptr.dir/main.cpp.o.provides: CMakeFiles/nullptr.dir/main.cpp.o.requires
	$(MAKE) -f CMakeFiles/nullptr.dir/build.make CMakeFiles/nullptr.dir/main.cpp.o.provides.build
.PHONY : CMakeFiles/nullptr.dir/main.cpp.o.provides

CMakeFiles/nullptr.dir/main.cpp.o.provides.build: CMakeFiles/nullptr.dir/main.cpp.o


CMakeFiles/nullptr.dir/tests.cpp.o: CMakeFiles/nullptr.dir/flags.make
CMakeFiles/nullptr.dir/tests.cpp.o: ../tests.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/dev/trainings/cpp-11-2019-05-22/nullptr/qtc-build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object CMakeFiles/nullptr.dir/tests.cpp.o"
	/usr/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/nullptr.dir/tests.cpp.o -c /home/dev/trainings/cpp-11-2019-05-22/nullptr/tests.cpp

CMakeFiles/nullptr.dir/tests.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/nullptr.dir/tests.cpp.i"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/dev/trainings/cpp-11-2019-05-22/nullptr/tests.cpp > CMakeFiles/nullptr.dir/tests.cpp.i

CMakeFiles/nullptr.dir/tests.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/nullptr.dir/tests.cpp.s"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/dev/trainings/cpp-11-2019-05-22/nullptr/tests.cpp -o CMakeFiles/nullptr.dir/tests.cpp.s

CMakeFiles/nullptr.dir/tests.cpp.o.requires:

.PHONY : CMakeFiles/nullptr.dir/tests.cpp.o.requires

CMakeFiles/nullptr.dir/tests.cpp.o.provides: CMakeFiles/nullptr.dir/tests.cpp.o.requires
	$(MAKE) -f CMakeFiles/nullptr.dir/build.make CMakeFiles/nullptr.dir/tests.cpp.o.provides.build
.PHONY : CMakeFiles/nullptr.dir/tests.cpp.o.provides

CMakeFiles/nullptr.dir/tests.cpp.o.provides.build: CMakeFiles/nullptr.dir/tests.cpp.o


# Object files for target nullptr
nullptr_OBJECTS = \
"CMakeFiles/nullptr.dir/main.cpp.o" \
"CMakeFiles/nullptr.dir/tests.cpp.o"

# External object files for target nullptr
nullptr_EXTERNAL_OBJECTS =

nullptr: CMakeFiles/nullptr.dir/main.cpp.o
nullptr: CMakeFiles/nullptr.dir/tests.cpp.o
nullptr: CMakeFiles/nullptr.dir/build.make
nullptr: CMakeFiles/nullptr.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/dev/trainings/cpp-11-2019-05-22/nullptr/qtc-build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Linking CXX executable nullptr"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/nullptr.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/nullptr.dir/build: nullptr

.PHONY : CMakeFiles/nullptr.dir/build

CMakeFiles/nullptr.dir/requires: CMakeFiles/nullptr.dir/main.cpp.o.requires
CMakeFiles/nullptr.dir/requires: CMakeFiles/nullptr.dir/tests.cpp.o.requires

.PHONY : CMakeFiles/nullptr.dir/requires

CMakeFiles/nullptr.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/nullptr.dir/cmake_clean.cmake
.PHONY : CMakeFiles/nullptr.dir/clean

CMakeFiles/nullptr.dir/depend:
	cd /home/dev/trainings/cpp-11-2019-05-22/nullptr/qtc-build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/dev/trainings/cpp-11-2019-05-22/nullptr /home/dev/trainings/cpp-11-2019-05-22/nullptr /home/dev/trainings/cpp-11-2019-05-22/nullptr/qtc-build /home/dev/trainings/cpp-11-2019-05-22/nullptr/qtc-build /home/dev/trainings/cpp-11-2019-05-22/nullptr/qtc-build/CMakeFiles/nullptr.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/nullptr.dir/depend

