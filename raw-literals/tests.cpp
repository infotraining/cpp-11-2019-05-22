#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("test")
{
    string str = "c:\\nasz katalog\\backup";
    string raw_string = R"(c:\nasz katalog\backup)";

    string multiline = R"(Line1
Line2
Line3)"
"\nLine4\nLine5";

    const char* tricky_one =
            R"k_raw(cytat dnia "(Kopiowanie jest ryzykowne...)" koniec cytatu)k_raw";

    cout << str << endl;
    cout << raw_string << endl;
    cout << multiline << endl;
    cout << tricky_one << endl;
}
