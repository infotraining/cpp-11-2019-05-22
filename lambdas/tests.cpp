#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <functional>

#include "catch.hpp"

using namespace std;

class Lambda_41236451768245
{
public:
    void operator()(int x) const { cout << "lambda for " << x << "\n"; }
};

class Lambda_Cpp14_With_Auto
{
public:
    template <typename T1, typename T2>
    bool operator()(const T1& a, const T2& b) { return *a < *b; };
};

bool cmp_ptrs(const std::unique_ptr<int>& a, const std::unique_ptr<int>& b)
{
    return *a < *b;
}

TEST_CASE("lambda")
{
    auto l1 = [](int x) { cout << "lambda for " << x << "\n"; };

    auto l_explain = Lambda_41236451768245{};

    auto multiply = [](int x, int y) { return x * y; };

    REQUIRE(multiply(3, 4) == 12);

    int (*ptr_fun)(int, int) = multiply;
    REQUIRE(ptr_fun(3, 5) == 15);

    [](){}();

    vector<unique_ptr<int>> vec;

    vec.push_back(make_unique<int>(13));
    vec.push_back(make_unique<int>(3));
    vec.push_back(make_unique<int>(11));

    // ok
    sort(begin(vec), end(vec),
         [](const std::unique_ptr<int>& a, const std::unique_ptr<int>& b) { return *a < *b; });

    SECTION("since C++14")
    {
        sort(begin(vec), end(vec),
             [](const auto& a, const auto& b) { return *a < *b; });
    }

    auto lambda_cmp_ptr = [](const std::unique_ptr<int>& a, const std::unique_ptr<int>& b) { return *a < *b; };

    // ok
    sort(begin(vec), end(vec), lambda_cmp_ptr);

    // less efficient
    sort(begin(vec), end(vec),
         &cmp_ptrs);

    for(const auto& ptr : vec)
        cout << *ptr << " ";
    cout << endl;
}

template <typename F>
void call(F f)
{
    f();
}

TEST_CASE("passing lambda as arg")
{
    call([]() { cout << "lambda" << endl; });
}

TEST_CASE("lambda with captures")
{
    int factor = 2;

    vector<int> vec = { 1, 2, 3, 4, 5 };

    auto mp = [factor](int x) { return x * factor; };

    factor += 10;

    transform(begin(vec), end(vec), begin(vec), mp);

    std::for_each(begin(vec),end(vec), [](int x) { cout << x << " ";} );
    cout << "\n";

    int sum = 0;

    for_each(begin(vec), end(vec), [&sum](int x) { sum += x; });
    cout << "sum: " << sum << endl;

    auto l1 = [=, &sum]() { sum += factor; };
    auto l2 = [&, factor]() { sum += factor; };

    int seed = 0;

    auto gen = [seed]() mutable { return ++seed; };

    vector<int> data(10);

    generate_n(begin(data), 10, gen);

    for(const auto& item : data)
    {
        cout << item << " ";
    }
    cout << "\n";
    cout << seed << endl;
}
