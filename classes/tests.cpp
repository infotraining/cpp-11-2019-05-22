#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <set>

#include "catch.hpp"

using namespace std;

struct Data
{
    int a = 0;
    double b{};
    string s = "unknown";

    Data(double b) : b{b}
    {}

    explicit Data(int a) : Data{3.14}
    {
    }

    Data(const Data& d) = delete;
    Data& operator=(const Data& d) = delete;
};

TEST_CASE("Data with initialized members")
{
    Data d{1};

    REQUIRE(d.s == "unknown"s);

    //Data backup = d; // ERROR - Data is non-copyable
}

struct Gadget
{
    int id = 0;

    Gadget() = default; // user-declared
    virtual ~Gadget() = default;

    explicit Gadget(const int id) : id{id}
    {
    }
};

TEST_CASE("classes")
{   
    Gadget g1{1};

    Gadget g2;

    REQUIRE(g2.id == 0);
}

void foo(int arg)
{
    cout << "foo(int: " << arg << ")\n";
}

void foo(double) = delete;

TEST_CASE("deleted function")
{
    foo(42);
    //foo(42.1);
}

class IndexedSet : public std::set<int>
{
public:
    IndexedSet(std::initializer_list<int> il) : std::set<int>(il)
    {
        cout << "Custom constructor" << endl;
    }

    using std::set<int>::set; // inheritance of constructors

    const int& operator[](size_t index) const
    {
        auto it = begin();
        advance(it, index);
        return *it;
    }
};

TEST_CASE("indexed set")
{
    IndexedSet s = { 5, 2, 7, 1, 2, 5 };
    s.insert(13);

    REQUIRE(s[1] == 2);

    vector<int> vec = { 1, 2, 3, 4, 5 };
    IndexedSet s2(vec.begin(), vec.end());
}
